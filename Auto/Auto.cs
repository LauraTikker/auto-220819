﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Auto
{
    class Auto
    {
        public static List<Auto> Autod = new List<Auto>();
        public static Dictionary<Auto, string> unikaalsedAutod = new Dictionary<Auto, string>();

        private static int autodeArv = 0;
        public string tootja;
        public string mudel;
        public int silindriteArv = 4;
        private readonly string numbrimärk;
        int number = ++autodeArv;

        public Auto(string numbrimärk)
        {
            this.numbrimärk = numbrimärk;
            Autod.Add(this);
            if (!unikaalsedAutod.ContainsValue(numbrimärk))
            {
                unikaalsedAutod.Add(this, numbrimärk);
            }
        }

        public override string ToString() => $"{number}. tootja: {tootja}, mudel: {mudel}, silindritearv: {silindriteArv}. ";
        
        public static void Print()
        {
            foreach (var x in unikaalsedAutod)
                Console.WriteLine(x.Value);
        }

    }
}
