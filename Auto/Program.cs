﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Auto
{
    class Program
    {
        static void Main(string[] args)
        {

            // info autodest loetakse failist:
            string autodefail = @"..\..\Autode nimekiri.txt";
            
            var loetudRead = File.ReadAllLines(autodefail); //võtab massiivi, mis sisaldab üksikuid ridu. var-i asemel võib olla ka string[]
            foreach (var x in loetudRead)
            {
                var jupid = x.Split(' ');
                Auto uusAuto = new Auto (jupid[3]) { tootja = jupid[0], mudel = jupid[1], silindriteArv = int.Parse(jupid[2])};
                Console.WriteLine(uusAuto.ToString());
            }


            // info autodest tuleb sisestada:
            Auto esimene = new Auto("123 FGR") { tootja = "BMW", mudel = "Kass", silindriteArv = 6 };
            Auto teine = new Auto("456 TYU") { tootja = "SAAB", mudel = "Pelican", silindriteArv = 3 };
            Auto kolmas = new Auto("908 FFU") { tootja = "AUDI", mudel = "A4" };
            Auto neljas = new Auto("908 FFU") { tootja = "AUDI", mudel = "A4", silindriteArv = 1 };

            Console.WriteLine(esimene.ToString());
            Console.WriteLine(teine.ToString());
            Console.WriteLine(kolmas.ToString());
            Console.WriteLine(neljas.ToString());


            Auto.Print();
            // static meetodi puhul pöördutakse klassi nime kaudu: Auto.PrindiAuto2(hennuAuto); parammeeter hennuAuto on this
            // hennuAuto.PrindiAuto1(), siin instansi meetodil on peidetud parameeter this, mis võetakse punkti eest


        }
    }
}
